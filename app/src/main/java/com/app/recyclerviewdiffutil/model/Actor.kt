package com.app.recyclerviewdiffutil.model

class Actor(val id: Int, val name: String, val rating: Int, val yearOfBirth: Int)